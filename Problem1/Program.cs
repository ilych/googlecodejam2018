﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Problem1
{
  internal class Program
  {
    public static void Main(string[] args)
    {
      var casesNum = int.Parse(Console.ReadLine());
      for (var i = 1; i <= casesNum; ++i)
      {
        var caseLength = int.Parse(Console.ReadLine());
        var caseNumbers = Console.ReadLine().Split(' ').Select(int.Parse);

        var index = SolveCase(caseNumbers, caseLength);

        Console.WriteLine("Case #{0}: {1}", i, index < 0 ? "OK" : index.ToString());
      }
    }

    private static int SolveCase(IEnumerable<int> numbers, int length)
    {
      int[] seq0, seq1;
      Split(numbers, length, out seq0, out seq1);

      Array.Sort(seq0);
      Array.Sort(seq1);

      return IndexOfError(seq0, seq1, length);
    }

    private static void Split(IEnumerable<int> numbers, int length, out int[] seq0, out int[] seq1)
    {
      var half = length / 2;
      seq0 = new int[length - half];
      seq1 = new int[half];
      var i = 0;
      foreach (var n in numbers)
      {
        var seq = (i % 2 == 0) ? seq0 : seq1;
        seq[i / 2] = n;
        ++i;
      }
    }

    private static int IndexOfError(int[] seq0, int[] seq1, int length)
    {
      for (var i = 0; i < length - 1; ++i)
      {
        if (GetAt(i, seq0, seq1) > GetAt(i + 1, seq0, seq1))
        {
          return i;
        }
      }

      return -1;
    }

    private static int GetAt(int index, int[] seq0, int[] seq1)
    {
      var seq = (index % 2 == 0) ? seq0 : seq1;
      return seq[index / 2];
    }
  }
}