﻿using System;
using System.Globalization;
using System.IO;

namespace Problem2
{
  internal class Program
  {
    public static void Main(string[] args)
    {
      if (args.Length > 0)
      {
        var stream = new FileStream(args[0], FileMode.Open, FileAccess.Read);
        Console.SetIn(new StreamReader(stream));
      }
      
      CultureInfo.CurrentCulture = new CultureInfo("en-US");
      
      var num = int.Parse(Console.ReadLine());
      for (var i = 1; i <= num; ++i)
      {
        var requiredArea = double.Parse(Console.ReadLine());

        var points = SolveCase(requiredArea);

        Console.WriteLine("Case #{0}:", i);
        foreach (var p in points)
        {
          Console.WriteLine("{0} {1} {2}", p.x, p.y, p.z);
        }
      }
    }

    private static Point[] SolveCase(double requiredArea)
    {
      var a = (requiredArea + Math.Sqrt(2.0 - requiredArea * requiredArea)) * 0.25;
      var b = 0.5 * (requiredArea - 2.0 * a);

      return new Point[]
      {
        new Point(a, b, 0),
        new Point(b, a, 0),
        new Point(0, 0, 0.5), 
      };
    }

    struct Point
    {
      public readonly double x, y, z;

      public Point(double x, double y, double z)
      {
        this.x = x;
        this.y = y;
        this.z = z;
      }
    }
  }
}