﻿using System;

namespace Problem0
{
  internal class Program
  {
    public static void Main(string[] args)
    {
      var num = int.Parse(Console.ReadLine());
      for (var i = 1; i <= num; ++i)
      {
        var caseArgs = Console.ReadLine().Split(' ');
        var caseHp = int.Parse(caseArgs[0]);
        var caseProgram = caseArgs[1];

        var hacks = SolveCase(caseHp, caseProgram);

        Console.WriteLine("Case #{0}: {1}", i, hacks < 0 ? "IMPOSSIBLE" : hacks.ToString());
      }
    }

    private static int SolveCase(int hp, string program)
    {
      int damage;
      var lookupHint = int.MaxValue;
      var code = Compile(program, out damage, ref lookupHint);
      var hacks = 0;
      while (hp < damage)
      {
        var hacked = Hack(code, ref damage, ref lookupHint);
        if (!hacked)
        {
          return -1;
        }

        ++hacks;
      }

      return hacks;
    }

    private static int[] Compile(string program, out int damage, ref int lookupHint)
    {
      var code = new int[program.Length];
      damage = 0;
      var power = 1;
      var lastKnownShot = 0;
      var i = 0;
      foreach (var op in program)
      {
        switch (op)
        {
          case 'C':
            power *= 2;
            code[i] = 0;
            break;
          case 'S':
            damage += power;
            code[i] = power;
            lastKnownShot = i;
            break;
          default:
            throw new ArgumentException(string.Format("Unexpected program op: {0}", op));
        }

        ++i;
      }

      lookupHint = lastKnownShot;
      return code;
    }

    private static bool Hack(int[] code, ref int damage, ref int lookupHint)
    {
      int? shotPower = null;
      for (var i = Math.Min(code.Length - 1, lookupHint); i >= 0; --i)
      {
        var op = code[i];
        if (op != 0)
        {
          if (shotPower == null)
          {
            lookupHint = i;
          }
          shotPower = op;
        }
        else if (shotPower.HasValue)
        {
          code[i] = shotPower.Value / 2;
          code[i + 1] = 0;
          damage -= shotPower.Value / 2;
          if (i + 1 == lookupHint)
          {
            lookupHint = i;
          }
          return true;
        }
      }
      return false;
    }
  }
}